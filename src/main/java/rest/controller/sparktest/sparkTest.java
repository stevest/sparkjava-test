/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rest.controller.sparktest;

import com.google.gson.Gson;
import gr.csd.uoc.cs359.winter2019.cart.data.CD;
import gr.csd.uoc.cs359.winter2019.cart.data.CDContainer;
import static spark.Spark.delete;
import static spark.Spark.get;
import static spark.Spark.options;
import static spark.Spark.post;
import static spark.Spark.put;

/**
 *
 * @author papadako
 */
public class sparkTest {

    private static final CDContainer container = new CDContainer();

    public static void main(String[] args) {

        // Adding  a new cd
        post("/cds", (request, response) -> {
            response.type("application/json");
            CD cd = new Gson().fromJson(request.body(), CD.class);

            if (container.addCD(cd)) {
                response.status(201);
                return new Gson()
                        .toJson(new CDResponse(CDResponse.ResponseEnum.SUCCESS,
                                "CD Added",
                                new Gson().toJsonTree(cd)));
            } else {
                response.status(400);
                return new Gson()
                        .toJson(new CDResponse(CDResponse.ResponseEnum.ERROR,
                                "Wrong CD Description",
                                new Gson().toJsonTree(cd)));
            }
        });

        // Wrong request
        post("/cds/:id", (request, response) -> {
            response.type("application/json");
            response.status(405);

            // We could also return supported methods for this resource...!!!
            return new Gson()
                    .toJson(new CDResponse(CDResponse.ResponseEnum.ERROR,
                            "POST not supported in non container URIs",
                            new Gson().toJsonTree("{}")));
        });

        // TODO Implementation should return the representation of the cd container
        get("/cds", (request, response) -> {
            response.status(200);
            return new Gson()
                    .toJson(new CDContainerResponse(CDContainerResponse.ResponseEnum.SUCCESS,
                            "CDs in container",
                            new Gson().toJsonTree(container)));
        });

        // Get representation of CD
        get("/cds/:id", (request, response) -> {

            response.type("application/json");
            CD cd = container.getCD(request.params(":id"));
            if (cd != null) {
                response.status(200);
                return new Gson()
                        .toJson(new CDResponse(CDResponse.ResponseEnum.SUCCESS,
                                "CD Retrieved",
                                new Gson().toJsonTree(cd)));
            } else {
                response.status(404);
                return new Gson()
                        .toJson(new CDResponse(CDResponse.ResponseEnum.ERROR,
                                "",
                                new Gson().toJsonTree("")));
            }
        });

        // TODO Updating a resource
        put("/cds/:id", (request, response) -> {
            return "";
        });

        // TODO deleting a resource
        delete("/cds/:id", (request, response) -> {
            return "";
        });

        // TODO Options (ask about existential things)
        options("/cds/:id", (request, response) -> {
            return "";
        });

        // TODO Options, return supported actions
        options("/cds/", (request, response) -> {
            return "";
        });
    }

}
