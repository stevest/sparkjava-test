/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.csd.uoc.cs359.winter2019.cart.data;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 *
 * @author papadako
 */
public class CDContainer {

    Map<String, CD> map = new HashMap<>();

    public boolean addCD(CD cd) {
        if ((cd == null) || (cd.getDescription() == null) || (cd.getPrice() == 0)) {
            return false;
        }

        String uniqueID = UUID.randomUUID().toString();
        cd.setId(uniqueID);
        map.put(cd.getId(), cd);
        return true;
    }

    public Map<String, CD> getCDs() {
        return map;
    }

    public CD getCD(String id) {
        return map.get(id);
    }

    public CD editCD(CD cd) {
        map.put(cd.getId(), cd);
        return cd;
    }

    public void deleteCD(String id) {
        map.remove(id);
    }

    public boolean exists(String id) {
        return map.containsKey(id);
    }
}
