/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gr.csd.uoc.cs359.winter2019.cart.data;

/**
 * FORTH-ICS
 *
 * @author Panagiotis Papadakos <papadako at ics.forth.gr>
 */
public class CD {

    private String description = null;
    private float price = 0;
    private String id;
    private String resource;

    public CD(String desc, float price, String id) {
        description = desc;
        this.price = price;
        this.id = id;
    }

    public void CD() {
        description = null;
        this.price = 0.0f;
        this.id = null;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public void setId(String id) {
        this.id = id;
        resource = "cds/" + id;
    }

    public String getResource() {
        return resource;
    }

    public float getPrice() {
        return price;
    }

    public String getId() {
        return id;
    }
}
